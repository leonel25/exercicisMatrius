let rows = document.querySelector("tbody").children
let matrix = []
for (var i = 0; i < rows.length; i++) {
    matrix.push(rows[i].children)
}
function paintAll() {

    for (var i = 0; i < rows.length; i++) {
        for (var j = 0; j < rows[i].cells.length; j++) {
            matrix[i][j].style.backgroundColor = "red";
        }
    }
}

function erase() {
    for (var i = 0; i < rows.length; i++) {
        for (var j = 0; j < rows[i].cells.length; j++) {
            matrix[i][j].style.backgroundColor = "white";
        }
    }
}

function paintRightHalf() {
    erase();
    for (var i = 0; i < rows.length; i++) {
        for (var j = rows[i].cells.length - Math.floor(rows[i].cells.length * 0.5); j < rows[i].cells.length; j++) {
            matrix[i][j].style.backgroundColor = "red";
        }
    }
}

function paintLeftHalf() {
    erase();

    for (var i = 0; i < rows.length; i++) {
        for (var j = 0; j < Math.ceil(rows[i].cells.length * 0.5); j++) {
            matrix[i][j].style.backgroundColor = "red";
        }
    }

}

function paintUpperHalf() {
    erase();
    for (var i = 0; i < Math.ceil(rows.length * 0.5); i++) {
        for (var j = 0; j < rows[i].cells.length; j++) {
            matrix[i][j].style.backgroundColor = "red";
        }
    }

}


function paintLowerTriangle() {
    erase();
    for (var i = 0; i < rows.length; i++) {
        for (var j = 0; j < rows[i].cells.length; j++) {
            if (i > j) {
                matrix[i][j].style.backgroundColor = "red";
            }
        }
    }
}

function paintUpperTriangle() {
    erase();
    for (var i = 0; i < rows.length; i++) {
        for(var j = 0; j < rows[i].cells.length; j++) { // afegir codi
            if(i <= j) 
            {
            matrix[i][j].style.backgroundColor = "red";
        }
    }

}
}


function paintPerimeter() {
    erase();
    for (var i = 0; i < rows.length; i++) {
        for(var j = 0; j < rows[i].cells.length; j++) {
            if(i == 0  || j == 0  || i == rows.length -1 || j == rows[i].cells.length -1) {
            // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }
}
}

function paintCheckerboard() {
    erase();
    for (var i = 0; i < rows.length; i++) {
        var startNumb = i % 2 == 0 ? 0 : 1
        for(var j = startNumb; j < rows[i].cells.length; j = j + 2) { 
            // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }
}

function paintCheckerboard2() {
    erase();
    for (var i = 0; i < rows.length; i++) {
        var startNumb = i % 2 == 0 ? 1 : 0
        for(var j = startNumb; j < rows[i].cells.length; j = j + 2) { 
            matrix[i][j].style.backgroundColor = "red";
        }
    }
}